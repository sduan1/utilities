﻿$buildlocation = "\\sea1fs01\Shared\Deliverables\Internal\Builds\Products\Globys\"
$releaselocation = "\\sea1fs01\Shared\Deliverables\External\Releases\Products\GLOBYS\"
$envlocationPerf = "\\sea1engfs01\Users\amak\3boxperf\"
$envlocation01 = "\\sea1engfs01\Users\amak\3boxconfignopschanges01\"
$envlocation02 = "\\sea1engfs01\Users\amak\3boxconfignopschanges02\"
$envlocation03 = "\\sea1engfs01\Users\amak\3boxconfignopschanges03\"


#To support 3 tier deployment, this function will go grab the build and copy the configuration files and rename the configuration files so that everything is ready to deploy.
#The configuration files are put in share according to which box you plan to deploy to.
#You will need a 3 tier core json for example att_e2e_core320.0.json which is located in \\sea1engfs01\Users\amak\3boxconfignopschanges01\ for the 01 box
#You will need cover_ven_vars.json from the build which you get from gitlab.com from the g3-core repo based on your build number and you put this file in for example \\sea1engfs01\Users\amak\320core_env_vars\328\core_env_vars.json
#You will need the json for the ATT CP env for example att_21.06env.json and you will get that from the build and you will put that file in for example \\sea1engfs01\Users\amak\3boxconfignopschanges01\3.5.0\237
#You will need to ensure the att_e2e_core320.0.json and att_21.06env.json have all the server names pointed to the right boxes 01, 02, or 03
#You just need to copy this powershell script onto each box and run it and you will have build ready for deployment and just need to run the deploy script to deploy core.
function Copy-Core ($envlocation, $release, $version, $build, $coreenvarsjsonfile, $corejson, $cpjsonlocation, $cpjsonfile) {

	if ($release) {
		$source = "$releaselocation$version"
		$target = "E:\ToDeploy\$version"

	}
	else {    
		$source = "$buildlocation$version\$build"
		$target = "E:\ToDeploy\$version.$build"        
	}
	Write-Host "$source copying to $target"
	robocopy $source $target /E /mt /z
	Copy-Item $coreenvarsjsonfile -Destination "$target\Deploy"
	Copy-Item "$envlocation\$corejson" -Destination "$target\Deploy\att_e2e_core.json"
	$cpjsonlocationPath = $envlocation + $cpjsonlocation
	Copy-Item "$cpjsonlocationPath\$cpjsonfile" -Destination "$target\Deploy\att_e2e-101.json"
	cd $target\Deploy
	#powershell.exe ".\Deploy.ps1 -deploy_specs ('.\core_pkg_vars.json', '.\core_product_vars.json', '.\core_env_vars.json', '.\att_e2e_core.json') -override_specs ('.\att_e2e-101.json')"

}
#Copy-Core $envlocation02 $true "3.18.1" "Release" "\\sea1engfs01\Users\amak\318.1core_env_vars\Release\core_env_vars.json" "att_e2e_core318.1.json" "3.3.6\Release\" "att_e2e-101.json"
#Copy-Core $envlocation02 $false "3.19.1" "28" "\\sea1engfs01\Users\amak\319.1core_env_vars\28\core_env_vars.json" "att_e2e_core319.1.json" "3.3.7\Release\" "att_e2e-101.json"
#Copy-Core $envlocation02 $false "3.19.1" "39" "\\sea1engfs01\Users\amak\319.1core_env_vars\39\core_env_vars.json" "att_e2e_core319.1.json" "3.3.7\Release\" "att_e2e-101.json"
#Copy-Core $envlocationPerf $true "3.18.1" "Release" "\\sea1engfs01\Users\amak\318.1core_env_vars\Release\core_env_vars.json" "att_e2e_core318.1.json" "3.3.7\Release\" "att_e2e-101.json"
#Copy-Core $envlocationPerf $false "3.19.1" "39" "\\sea1engfs01\Users\amak\319.1core_env_vars\39\core_env_vars.json" "att_e2e_core319.1.json" "3.3.7\Release\" "att_e2e-101.json"
#Copy-Core $envlocation03 $true "3.18.0" "Release" "\\sea1engfs01\Users\amak\318core_env_vars\Release\core_env_vars.json" "att_e2e_core318.0.json" "3.3.0\Release\" "att_e2e-101.json"
#Copy-Core $envlocation03 $true "3.18.1" "Release" "\\sea1engfs01\Users\amak\318.1core_env_vars\Release\core_env_vars.json" "att_e2e_core318.1.json" "3.3.7\Release\" "att_e2e-101.json"
#Copy-Core $envlocation03 $false "3.19.1" "42" "\\sea1engfs01\Users\amak\319.1core_env_vars\39\core_env_vars.json" "att_e2e_core319.1.json" "3.3.7\Release\" "att_e2e-101.json"
#Copy-Core $envlocation02 $false "3.19.1" "51" "\\sea1engfs01\Users\amak\319.1core_env_vars\51\core_env_vars.json" "att_e2e_core319.1.json" "3.4.0\129\" "att_e2e_sample.json"
#Copy-Core $envlocation01 $true "3.19.1" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\51\core_env_vars.json" "att_e2e_core319.1.json" "3.4.0\Release\" "att_e2e_sample.json"
#Copy-Core $envlocation01 $true "3.19.1" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\51\core_env_vars.json" "att_e2e_core319.1.json" "3.4.0\Release\" "att_e2e_sample.json"
#Copy-Core $envlocation02 $true "3.19.1" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "3.4.0\Release\" "att_e2e_sample.json"
#Copy-Core $envlocation01 $false "3.20.0" "143" "\\sea1engfs01\Users\amak\320core_env_vars\143\core_env_vars.json" "att_e2e_core320.0.json" "3.4.3\8\" "att_e2e_sample.json"
#Copy-Core $envlocation01 $false "3.20.0" "174" "\\sea1engfs01\Users\amak\320core_env_vars\174\core_env_vars.json" "att_e2e_core320.0.json" "3.4.4\Release\" "att_e2e_sample.json"
#Copy-Core $envlocation02 $true "3.19.1" "Release" "\\sea1engfs01\Users\amak\319.1core_env_vars\52\core_env_vars.json" "att_e2e_core319.1.json" "3.4.0\Release\" "att_e2e_sample.json"
#Copy-Core $envlocation02 $false "3.20.0" "207" "\\sea1engfs01\Users\amak\320core_env_vars\207\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\125\" "att_21.06env.json"
#Copy-Core $envlocation01 $false "3.20.0" "219" "\\sea1engfs01\Users\amak\320core_env_vars\219\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\159\" "att_21.06env.json"
#Copy-Core $envlocation02 $false "3.20.0" "222" "\\sea1engfs01\Users\amak\320core_env_vars\222\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\163" "att_21.06env.json"
#Copy-Core $envlocation02 $false "3.20.0" "244" "\\sea1engfs01\Users\amak\320core_env_vars\244\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\172" "att_21.06env.json"
#Copy-Core $envlocation01 $false "3.20.0" "257" "\\sea1engfs01\Users\amak\320core_env_vars\257\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\186" "att_21.06env.json"
#Copy-Core $envlocation02 $false "3.20.0" "290" "\\sea1engfs01\Users\amak\320core_env_vars\290\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\220" "att_21.06env.json"
#Copy-Core $envlocation01 $false "3.20.0" "314" "\\sea1engfs01\Users\amak\320core_env_vars\314\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\237" "att_21.06env.json"
#Copy-Core $envlocation02 $false "3.20.0" "328" "\\sea1engfs01\Users\amak\320core_env_vars\328\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\248" "att_21.06env.json"
Copy-Core $envlocation01 $false "3.20.0" "384" "\\sea1engfs01\Users\amak\320core_env_vars\384\core_env_vars.json" "att_e2e_core320.0.json" "3.5.0\283" "att_21.08env.json"