import requests
import time
import json
import threading
from datetime import datetime


def getPaperless (bearerToken):
    threadname = threading.currentThread().getName()
    endpoint = "https://imtelus1081.corp.ad.local/telus/frontend/organization/v1/organization/0000902806/accounts/paperless"
    headers = {"Authorization": bearerToken}
    for x in range(1, 1001):
        now = datetime.now()
        print (requests.get(endpoint, headers=headers, verify=False))
        print("{} {}:  Finished {} of 1000".format(now,threadname,x))
        time.sleep(0.200)

posturl = "https://imtelus1081.corp.ad.local/telus/frontend/auth/oidc/connect/token"

# data to be sent to api 
data = {'grant_type':'client_credentials', 
        'client_id':'paymentsFrontend', 
        'client_secret':'this_is_the_secret', 
        'scope':'Paperless'} 

r = requests.post(url = posturl, data = data, verify=False)
jsondata = json.loads(r.text)
accessToken = jsondata["access_token"]
bearer = "Bearer " + accessToken

# creating thread
t1 = threading.Thread(name='Thread 1', target=getPaperless, args=(bearer,))
t2 = threading.Thread(name='Thread 2', target=getPaperless, args=(bearer,))
t3 = threading.Thread(name='Thread 3', target=getPaperless, args=(bearer,))
t4 = threading.Thread(name='Thread 4', target=getPaperless, args=(bearer,))
t5 = threading.Thread(name='Thread 5', target=getPaperless, args=(bearer,))
t6 = threading.Thread(name='Thread 6', target=getPaperless, args=(bearer,))
t7 = threading.Thread(name='Thread 7', target=getPaperless, args=(bearer,))
t8 = threading.Thread(name='Thread 8', target=getPaperless, args=(bearer,))
t9 = threading.Thread(name='Thread 9', target=getPaperless, args=(bearer,))

# starting threads
t1.start()
t2.start()
t3.start()
t4.start()
t5.start()
t6.start()
t7.start()
t8.start()
t9.start()
 
# wait until threads are completed
t1.join()
t2.join()
t3.join()
t4.join()
t5.join()
t6.join()
t7.join()
t8.join()
t9.join()
 
# both threads completely executed
print("Done!")