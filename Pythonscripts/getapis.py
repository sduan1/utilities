import requests
import time
import json
import threading
from datetime import datetime

def getPaperless ():
    # data to be sent to api 
    for x in range(1, 1001):
        error = 0

        posturl = "https://IMTELUS1081.corp.ad.local/TELUS/services/AuthService/oidc/connect/token"
        data = {'grant_type':'client_credentials', 
        'client_id':'services', 
        'client_secret':'this_is_the_secret', 
        'scope':'services'} 

        r = requests.post(url = posturl, data = data, verify=False)
        jsondata = json.loads(r.text)
        accessToken = jsondata["access_token"]
        bearerToken = "Bearer " + accessToken
        time.sleep(0.300)
        threadname = threading.currentThread().getName()
        endpoint = "https://IMTELUS1081.corp.ad.local/TELUS/services/OrganizationService/users/2991169/culture"
        anotherendpt = "https://IMTELUS1081.corp.ad.local/TELUS/services/OrganizationService/users/2991169/displayFormats"
        headers = {"Authorization": bearerToken}
    
        now = datetime.now()
        myResponse = requests.get(endpoint, headers=headers, verify=False)
        myStatusCode = myResponse.status_code     
        myResponse1 = requests.get(anotherendpt, headers=headers, verify=False)
        myStatusCode1 = myResponse.status_code     
        if ( myStatusCode != requests.codes.ok or myStatusCode1 != requests.codes.ok):
            print("ERROR")
            error += 1
        
        if (x > 990):
            print("{} {}:  Finished {} of 1000".format(now,threadname,x))
        time.sleep(0.100)
    end = datetime.now()
    print("*************{}:  Start time:  {} End time: {}  Finished {} of 1000  Number of errors {}".format(threadname,now,end,x,error))

start = datetime.now()
# creating thread
t1 = threading.Thread(name='Thread 1', target=getPaperless)
time.sleep(10)
t1.start()
t2 = threading.Thread(name='Thread 2', target=getPaperless)
time.sleep(60)
t2.start()
t3 = threading.Thread(name='Thread 3', target=getPaperless)
time.sleep(120)
t3.start()
t4 = threading.Thread(name='Thread 4', target=getPaperless)
time.sleep(180)
t4.start()
t5 = threading.Thread(name='Thread 5', target=getPaperless)
time.sleep(240)
t5.start()
t6 = threading.Thread(name='Thread 6', target=getPaperless)
time.sleep(300)
t6.start()
t7 = threading.Thread(name='Thread 7', target=getPaperless)
time.sleep(360)
t7.start()
t8 = threading.Thread(name='Thread 8', target=getPaperless)
time.sleep(400)
t8.start()
t9 = threading.Thread(name='Thread 9', target=getPaperless)
time.sleep(460)
t9.start()
 
# wait until threads are completed
t1.join()
t2.join()
t3.join()
t4.join()
t5.join()
t6.join()
t7.join()
t8.join()
t9.join()
 
finish = datetime.now() 
# both threads completely executed
print("Start:  {}  Finish:  {}".format(start, finish))
print("Done")

