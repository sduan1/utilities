import requests
import time
import json


def getPaperless (bearerToken):
    #endpoint = "https://imtelus1095.corp.ad.local/telus/frontend/reports/v1/dashboard/getMonthlySpendReport"
    #endpoint = "https://IMTELUS1095.corp.ad.local/TELUS/services/ReportsService/dashboard/getMonthlySpendReport"
    endpoint = "https://IMTELUS1095.corp.ad.local/TELUS/services/OrganizationService/users/2991169/culture"
 
    headers = {"Authorization": bearerToken}
    myResponse = requests.get(endpoint, headers=headers, verify=False)
    myStatusCode = myResponse.status_code     
    if ( myStatusCode == requests.codes.ok):
        print("OK")
    else :
        print("ERROR")

#posturl = "https://imtelus1095.corp.ad.local/telus/frontend/auth/oidc/connect/token"
posturl = "https://IMTELUS1095.corp.ad.local/TELUS/services/AuthService/oidc/connect/token"

# data to be sent to api 
data = {'grant_type':'client_credentials', 
        'client_id':'services', 
        'client_secret':'this_is_the_secret', 
        'scope':'services'} 

r = requests.post(url = posturl, data = data, verify=False)
jsondata = json.loads(r.text)
print(jsondata)
accessToken = jsondata["access_token"]
bearer = "Bearer " + accessToken
getPaperless(bearer)


#grant_type=client_credentials&client_id=services&client_secret=this_is_the_secret&scope=services
#grant_type=client_credentials&client_id=services&client_secret=this_is_the_secret&scope=services